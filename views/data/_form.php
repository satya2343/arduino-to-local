<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Data */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'node1')->textInput() ?>

    <?= $form->field($model, 'node2')->textInput() ?>

    <?= $form->field($model, 'node3')->textInput() ?>

    <?= $form->field($model, 'time_request')->textInput() ?>

    <?= $form->field($model, 'time_received')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
