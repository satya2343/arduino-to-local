<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data".
 *
 * @property int $id
 * @property double $node1
 * @property double $node2
 * @property double $node3
 * @property string $time_request
 * @property string $time_received
 */
class Data extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['node1', 'node2', 'node3'], 'required'],
            [['id'], 'integer'],
            [['node1', 'node2', 'node3'], 'number'],
            [['time_request', 'time_received'], 'safe'],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'node1' => 'Node1',
            'node2' => 'Node2',
            'node3' => 'Node3',
            'time_request' => 'Time Request',
            'time_received' => 'Time Received',
        ];
    }
}
