/*
  Web client

 This sketch connects to a website (http://www.google.com)
 using an Arduino Wiznet Ethernet shield.

 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13

 created 18 Dec 2009
 by David A. Mellis
 modified 9 Apr 2012
 by Tom Igoe, based on work by Adrian McEwen

 */

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
IPAddress server(192,168,137,1);  // numeric IP for Google (no DNS)
//char server[] = "http://gatot.clg";    // name address for Google (using DNS)

// Set the static IP address to use if the DHCP fails to assign
IPAddress ip(192, 168, 137, 27);

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // start the Ethernet connection:
//  if (Ethernet.begin(mac) == 0) {
//    Serial.println("Failed to configure Ethernet using DHCP");
    // try to congifure using IP address instead of DHCP:
    
//  }
  Serial.println("initializing...");
  Ethernet.begin(mac, ip);
  // give the Ethernet shield a second to initialize:
  delay(5000);
  Serial.println("connecting...");

  // if you get a connection, report back via serial:
  
}

void loop() {
  sentRequest();
  while (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  delay(2000);
}

void sentRequest()
{
   if (client.connect(server, 80)) {
    Serial.println("connected");
    // Make a HTTP request:
//    client.println("GET /gatot/web/index.php?r=api/insert-data&node1=5.0&node2=4.9&node3=4.7&time_request=0&time_received=0");
    client.print("GET /gatot/web/index.php?r=api/insert-data&node1=");
    //NODE1
    client.print(analogRead(1));
    //NODE2
    client.print("&node2=");
    client.print(analogRead(2));
    //NODE3
    client.print("&node3=");
    client.print(analogRead(3));
    //T0
    client.print("&time_request=");
    client.print(millis());
    //T1
    client.print("&time_received=");
    client.println(millis());
    
    client.println("Host: 192.168.137.1");
    client.println("Connection: close");
    client.println();
    client.stop();
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

