<?php
/**
 * Created by PhpStorm.
 * User: satya
 * Date: 4/28/2018
 * Time: 12:29 AM
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Data;

class ApiController extends Controller
{
    public function actionTest()
    {
        return 'ngopo su?';
    }

    public function actionInsertData($node1 = null, $node2 = null, $node3 = null, $time_request = null, $time_received = null)
    {
        $data = new Data();
        $data->node1 = $node1;
        $data->node2 = $node2;
        $data->node3 = $node3;
        $data->time_request = $time_request;
        $data->time_received = $time_received;

        if ($data->save()){
            return 'Success';
        }else{
            return 'Something Wrong?';
        }
    }
}